function log(message, plugin) {
  const title = plugin || 'DMod';
  console.log(`%c[${title}] %c${message}`, 'color: green; font-weight: bold;', 'color: inherit; font-weight: normal;')
}

module.exports.log = log;