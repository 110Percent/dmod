const { ipcRenderer } = require('electron');
const path = require('path');
const logger = require('./logger');
let package = require('../package.json');

global.__base = path.join(__dirname, '../');

const originalPreload = ipcRenderer.sendSync('mod', 'fetchPreload');
require(originalPreload);

console.log(package)
for (let i = 0; i < package['dmod-plugins'].length; i++) {
  try {
    require(`../plugins/${package['dmod-plugins'][i]}`);
    logger.log(`Loaded plugin ${package['dmod-plugins'][i]}`);
  } catch (err) {
    logger.log(`Error loading plugin ${package['dmod-plugins'][i]}\n${err}`);
  }
}