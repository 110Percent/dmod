const asar = require('asar');
const path = require('path');
const semver = require('semver');
const fs = require('fs');
const os = require('os');
const ps = require('ps-node');

let config = require('./config.json');
if (!config.desktopDirs) config.desktopDirs = {};

const configPaths = {
    'linux': {
        'Discord': path.join(os.homedir(), '/.config/discord'),
        'DiscordPTB': path.join(os.homedir(), '/.config/discordptb'),
        'DiscordCanary': path.join(os.homedir(), '/.config/discordcanary')
    },
    'win32': {
        'Discord': path.join(os.homedir(), '/AppData/Roaming/discord'),
        'DiscordPTB': path.join(os.homedir(), '/AppData/Roaming/discordptb'),
        'DiscordCanary': path.join(os.homedir(), '/AppData/Roaming/discordcanary')
    }
}

ps.lookup({
    command: 'discord',
    arguments: '-e',
}, function(err, resultList) {
    if (err) {
        throw new Error(err);
    }
    resultList = resultList.filter(function(item, pos) {
        return resultList.map(c => c.command).indexOf(item.command) == pos;
    })

    if (resultList.length < 1) {

    } else if (resultList.length === 1) {
        let command = resultList[0].command.split('/')[resultList[0].command.split('/').length - 1];
        command = command.split('\\')[command.split('\\').length - 1];
        command = command.split('.')[0]
        handleInject(command, resultList[0].command);
    }
});

function handleInject(discordProcess, procPath) {

    console.log(discordProcess)

    // Inject base script into main Discord script
    console.log('Starting Base');
    let procDir = path.dirname(procPath);
    asar.extractAll(path.join(procDir, 'resources/app.asar'), path.join(procDir, 'resources/app'));
    let baseFile = fs.readFileSync(path.join(procDir, 'resources/app/app_bootstrap/index.js'), 'utf8')
    baseFile = baseFile + `\nrequire('${path.join(__dirname, 'index.js').replace(/\\/g, '\\\\')}')`;
    fs.writeFileSync(path.join(procDir, 'resources/app/app_bootstrap/index.js'), baseFile);
    asar.createPackage(path.join(procDir, 'resources/app'), path.join(procDir, 'resources/app.asar'))

    console.log('Done Base. Starting Desktop')

    // Replace preload script in Discord BrowserWindow with DMod's core script
    // Save location of core asar file to re-require original preload on run
    let confDir = configPaths[process.platform][discordProcess];
    let confList = fs.readdirSync(confDir).map(c => c.split('-')[c.split('-').length - 1]);
    let vers = confList.filter(c => semver.valid(c));
    let maxVer = vers.sort(semver.rcompare)[0];
    let desktopDir = path.join(confDir, maxVer, 'modules/discord_desktop_core');
    config.desktopDirs[discordProcess] = desktopDir;
    fs.writeFileSync(path.join(__dirname, 'config.json'), JSON.stringify(config, null, 2))
    asar.extractAll(path.join(desktopDir, 'core.asar'), path.join(desktopDir, 'core'));
    let mainScreen = fs.readFileSync(path.join(desktopDir, 'core/app/mainScreen.js'), 'utf8');
    mainScreen = mainScreen.replace("preload: _path2.default.join(__dirname, 'mainScreenPreload.js')", `preload: '${path.join(__dirname, 'core/index.js').replace(/\\/g, '\\\\')}'`);
    fs.writeFileSync(path.join(desktopDir, 'core/app/mainScreen.js'), mainScreen);
    asar.createPackage(path.join(desktopDir, 'core'), path.join(desktopDir, 'core.asar'));

    console.log('Done. Restart Discord.');
}