/*
 * settings - WIP Base plugin to modify the settings menu
 * Curtis "110Percent" Davies 2019
 */

const mutation = require('./mutation');
const logger = require(`${__base}/core/logger`);
const fs = require('fs');
const path = require('path');

// Create a DOM element to add a new category to the settings menu
const sidebarBaseRaw = fs.readFileSync(path.join(__base, 'html/settingsSidebarBase.html'), 'utf8');
const sidebarBase = sidebarBaseRaw.split('\n').map(c => new DOMParser().parseFromString(c, 'text/html').firstChild.lastChild.firstChild);

// Inject the category when the settings pane is opened
mutation.on('settingsOpen', (settingsPanel) => {
  const sidebar = settingsPanel.querySelector('[class*="side-"]');
  const placeholder = sidebar.querySelectorAll('[class*="separator-"]')[2];
  for (let i = 0; i < sidebarBase.length; i++) {
    sidebar.insertBefore(sidebarBase[i], placeholder);
  }
});