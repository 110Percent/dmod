/*
 * css - Base plugin to hotload a css file into the client
 * Curtis "110Percent" Davies 2019
 */

const logger = require(`${__base}/core/logger`);
const fs = require('fs');
const path = require('path');

let config = require(`${__base}/config.json`);

window.onload = () => {
  // Create custom style tag 
  let customStyle = document.createElement('style');
  customStyle.id = 'dmod-style';
  document.body.appendChild(customStyle);

  // Load stylesheet
  let cssPath;
  if (config.cssFile.startsWith('/')) {
    cssPath = path.resolve(config.cssFile);
  } else {
    cssPath = path.join(__dirname, '../..', config.cssFile)
  }
  try {
    customStyle.innerHTML = fs.readFileSync(cssPath);
    logger.log('Loaded stylesheet');
  } catch (err) {
    logger.log('Error loading stylesheet\n' + err);
    return;
  }

  // Watch css file for changes and automatically update style tag
  fs.watch(path.join(cssPath), _file => {
    customStyle.innerHTML = fs.readFileSync(cssPath);
    logger.log('Refreshed CSS')
  });
}