/*
 * mutation - Base plugin to handle changes in the Discord DOM
 * Curtis "110Percent" Davies 2019
 */

const { MutationObserver } = window;
const EventEmitter = require('events');
const logger = require(`${__base}/core/logger`);

class Mutator extends EventEmitter {

  constructor() {
    super();
    // Bind methods to the EventEmitter
    this.handleMutate = this.handleMutate.bind(this);
    this.handleGroup = this.handleGroup.bind(this);
    // Create a nwew MutationObserver and fire handleMutate on body mutation
    this.mutate = new MutationObserver(this.handleMutate);
    window.addEventListener('load', () => {
      try {
        this.mutate.observe(document.body, {
          attributes: true,
          childList: true,
          subtree: true
        });
        logger.log('Initialized mutation observer');
      } catch (err) {
        logger.log(`Failed to initialize mutation observer\n${err}`);
      }
    })
  }

  handleMutate(muts) {
    muts.forEach(mut => {
      for (var i = 0; i < mut.addedNodes.length; i++) {
        // Emit events based on specific mutation cases.
        // TODO: Add more cases
        if (!mut.addedNodes[i].getAttribute)
          continue;
        if (mut.addedNodes[i].getAttribute('aria-label') === 'USER_SETTINGS') {
          this.emit('settingsOpen', mut.addedNodes[i]);
          return;
        }
        if (typeof mut.addedNodes[i].className.split !== 'function')
          continue
        if (mut.addedNodes[i].className.split(' ').some(c => /message-.*/.test(c))) {
          this.emit('message', muts.addedNodes[i]);
          return;
        }
        if (mut.addedNodes[i].className.split(' ').some(c => /containerCozyBounded-.*/.test(c))) {
          this.handleGroup(mut.addedNodes[i]);
        }
        if (mut.addedNodes[i].className.split(' ').some(c => /messagesWrapper-.*/.test(c))) {
          mut.addedNodes[i].querySelectorAll('[class*="containerCozyBounded"]').forEach(e => {
            this.handleGroup(e);
          })
        }
      }
    });
  }

  handleGroup(element) {
    this.emit('messageGroup', element);
    let messages = element.querySelectorAll('[class*="message-"]');
    for (let i = 0; i < messages.length; i++) {
      this.emit('message', messages[i]);
    }
  }
}

module.exports = new Mutator();