// Base script - Injected into main Discord script

const path = require('path');
const electron = require('electron');
const config = require('./config.json');
const { BrowserWindow, ipcMain, app } = electron;
const electronCacheEntry = require.cache[require.resolve('electron')]

// Get build version of Discord
let execPath = require.main.filename.toLowerCase();
let discordBuild = 'Discord';
if (execPath.indexOf('canary') > -1) {
  discordBuild = 'DiscordCanary';
} else if (execPath.indexOf('ptb') > -1) {
  discordBuild = 'DiscordPTB';
}

electron.app.on('ready', () => {

  // Code borrowed from DiscordInjections repository
  // Patch webRequests to remove Content Security Policy headers
  electron.session.defaultSession.webRequest.onHeadersReceived(
    (details, done) => {
      const responseHeaders = Object.assign({}, details.responseHeaders)
      Object.keys(responseHeaders)
        .filter(k => k.toLowerCase().startsWith('content-security-policy'))
        .forEach(k => delete responseHeaders[k])
      done({ cancel: false, responseHeaders: responseHeaders })
    }
  )

  // Also borrowed from DiscordInjections
  // patch webRequest session to not ask for source map files
  electron.session.defaultSession.webRequest.onBeforeRequest((details, done) =>
    done({ cancel: details.url.endsWith('.js.map') })
  )
})

// On load, re-require the original Discord preload script
ipcMain.on('mod', (ev, arg) => {
  if (arg === 'fetchPreload') {
    let preload = ev.returnValue = path.join(config.desktopDirs[discordBuild], 'core.asar/app/mainScreenPreload.js');
    console.log(preload);
    ev.returnValue = preload;
  }
})